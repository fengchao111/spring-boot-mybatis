package com.lgy.common.base;

import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface IBaseService<T> {
    Mapper<T> getMapper();
    T add(T t);
    void delete(T t);
    int deleteByIds(List<Integer> ids);
    int update(T t);
    List<T> queryList(T t);
    int batchSave(List<T> list);
}
