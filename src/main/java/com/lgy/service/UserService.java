package com.lgy.service;

import com.lgy.common.base.IBaseService;
import com.lgy.model.User;

public interface UserService extends IBaseService<User> {
	int addUser(User user);
}
