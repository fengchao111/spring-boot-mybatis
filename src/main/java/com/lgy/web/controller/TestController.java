package com.lgy.web.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lgy.dao.UserMapper;
import com.lgy.model.User;
import com.lgy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("test")
public class TestController {
	@Autowired
	private UserService userService;

	@RequestMapping("")
	public String test() {
		System.out.println("调用了test");
		return "indesx";
	}
	
	@RequestMapping("jsp")
	public String testJsp() {
	    List<Integer> param = new ArrayList<>();
        param.add(1);
	    userService.deleteByIds(param);
		UserMapper mapper = (UserMapper) userService.getMapper();
		List<User> list = mapper.selectAll();
		System.out.println(list);
		System.out.println("调用了test");
		System.out.println("调用了test");
		System.out.println("调用了test");
		System.out.println("调用了test");
		return "index";
	}

	@RequestMapping("testTrans")
	@ResponseBody
	public String testTrans() {
		List<User> list = new ArrayList<>();
		User u = new User("111",11);
		User u1 = new User("222",222);
		User u2 = new User("333",33);
		list.add(u);
		list.add(u1);
		list.add(u2);
		int i = userService.batchSave(list);
		return "index------->" + i;
	}


	@RequestMapping("testPage")
	@ResponseBody
	public Object testTrans(int page,int pageSize) {
		PageHelper.startPage(page,pageSize,false);
		List<User> list = userService.queryList(new User(null,null));
		PageInfo pageInfo = new PageInfo(list);
		return pageInfo;
	}
}
