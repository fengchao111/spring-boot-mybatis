package com.lgy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.lgy.dao")
public class ApplicaitonMain {

	public static void main(String[] args) {
		SpringApplication.run(ApplicaitonMain.class, args);
	}

}
