package com.lgy.dao;

import java.util.List;

import com.lgy.model.User;

import com.lgy.common.base.IBaseDao;

public interface UserMapper extends IBaseDao<User> {
	User findOneById(Integer id);
	
	List<User> findList();
}
